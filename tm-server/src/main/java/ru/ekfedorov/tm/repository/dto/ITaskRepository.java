package ru.ekfedorov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ekfedorov.tm.api.repository.IRepository;
import ru.ekfedorov.tm.dto.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IRepository<Task> {

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :taskId")
    void bindTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("taskId") @NotNull String taskId
    );

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<Task> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<Task> findOneByUserIdAndId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Optional<Task> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    void removeAllByUserIdAndProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneByUserIdAndId(@Nullable String userId, @NotNull String id);

    void removeOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskFromProjectId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

}