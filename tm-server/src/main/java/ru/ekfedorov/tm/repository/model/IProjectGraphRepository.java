package ru.ekfedorov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IGraphRepository;
import ru.ekfedorov.tm.model.ProjectGraph;

import java.util.List;
import java.util.Optional;

public interface IProjectGraphRepository extends IGraphRepository<ProjectGraph> {

    void removeByUserId(@NotNull String userId);

    @NotNull
    List<ProjectGraph> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<ProjectGraph> findOneByUserIdAndId(
            @Nullable String userId, @NotNull String id
    );

    Optional<ProjectGraph> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    void removeOneByUserIdAndId(@Nullable String userId, @NotNull String id);

    void removeOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

}
