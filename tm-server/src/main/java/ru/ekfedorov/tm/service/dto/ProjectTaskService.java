package ru.ekfedorov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.repository.dto.IProjectRepository;
import ru.ekfedorov.tm.repository.dto.ITaskRepository;
import ru.ekfedorov.tm.api.service.dto.IProjectTaskService;
import ru.ekfedorov.tm.dto.Task;
import ru.ekfedorov.tm.exception.empty.ProjectIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.TaskIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @NotNull
    @Autowired
    public ITaskRepository taskRepository;

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @SneakyThrows
    @Override
    @Transactional
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        taskRepository.bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public List<Task> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        taskRepository.removeAllByUserIdAndProjectId(userId, projectId);
        projectRepository.removeOneByUserIdAndId(userId, projectId);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        taskRepository.unbindTaskFromProjectId(userId, taskId);
    }

}
