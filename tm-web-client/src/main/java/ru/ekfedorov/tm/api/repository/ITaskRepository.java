package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void create();

    void removeById(String id);

    List<Task> findAll();

    Task findById(String id);

    void save(Task task);

    void saveAll(List<Task> list);

    void removeAll();

}
