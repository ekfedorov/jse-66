package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

}

