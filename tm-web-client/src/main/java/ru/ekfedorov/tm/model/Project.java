package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractBusinessEntity {

    public Project(String name, final String description) {
        this.name = name;
        this.description = description;
    }

}

