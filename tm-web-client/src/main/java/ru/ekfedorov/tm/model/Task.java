package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity {

    private String projectId;

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}
