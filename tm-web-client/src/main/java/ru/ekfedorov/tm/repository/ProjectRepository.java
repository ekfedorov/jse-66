package ru.ekfedorov.tm.repository;

import org.springframework.stereotype.Repository;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.model.Project;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class ProjectRepository implements IProjectRepository {

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        create();
        create();
        create();
    }

    @Override
    public void create() {
        Project project = new Project("project", "new");
        projects.put(project.getId(), project);
    }

    @Override
    public void removeById(final String id) {
        projects.remove(id);
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values()) ;
    }

    @Override
    public Project findById(final String id) {
        return projects.get(id);
    }

    @Override
    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void saveAll(final List<Project> list) {
        projects.putAll(list.stream().collect(Collectors.toMap(Project::getId, Function.identity())));
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

}

